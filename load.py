# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import django
from pprint import pprint


def main():
    from app import models

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'main.settings')
    django.setup()
    main()

